/*EJERCICIO REALIZA OPERACIONES DE CRUD EN TIEMPO REAL SOBRE UN ARRAYLIST*/
/*MOSTRANDO EL RESULTADO DE LAS OPERACIONES SOBRE UN JTable*/
/*AUTOR JClearSky*/
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ArrayJava extends JFrame implements ActionListener, MouseListener{//Heredamos la clase JFrame e Implementamos Action Listener

	//Declaracion de elementos a utilizar
	private JLabel lblIndice, lblMencion, lblNombre, lblApellido; 			//Etiquetas
	private JTextField txtIndice, txtMencion, txtNombre, txtApellido; 	//Cajas de texto
	private JButton btnAgregar, btnModificar, btnEliminar, btnLimpiar;	//Botones
	private JScrollPane conTabla; 																			//Contenedor con scroll
	private JTable tblContenido;																				//Tabla para mostrar contenido
	private DefaultTableModel modTabla;                     						//Modelo de la tabla contenido
	private ArrayList<String> array_Estudiantes = new ArrayList<String>();

	public ArrayJava(){

		super();							//Llamada al constructor de la clase padre
		conf_Ventana();				//Llamada al metodo configurar ventana
		inic_Componentes();		//Llamada al metodo para iniciar los componentes de la ventana

	}

	private void conf_Ventana(){

		this.setTitle("Ejemplo Clase ArrayList + Swing");    //Definimos un titulo para el JFrame
		this.setSize(480, 320);		                        	 //Definimos un tamaño (x,y)
		this.setLocationRelativeTo(null);                    //Centramos la ventana
		this.setLayout(null);							    							 //No usamos ningun layout. Para posicionar elementos manualmente
		this.setResizable(false);                            //Evitamos que la ventana sea redimencionable
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Definimos que al cerrar la ventana se termine todo proceso asociado

	}

	private void inic_Componentes(){
		//Iniciamos los componentes
		lblIndice = new JLabel();
		lblMencion = new JLabel();
		lblNombre = new JLabel();
		lblApellido = new JLabel();

		txtIndice = new JTextField();
		txtMencion = new JTextField();
		txtNombre = new JTextField();
		txtApellido = new JTextField();

		btnAgregar = new JButton();
		btnModificar = new JButton();
		btnEliminar = new JButton();
		btnLimpiar = new JButton();

		conTabla = new JScrollPane();
		tblContenido = new JTable();
		modTabla = new DefaultTableModel();

		//Configuramos los componentes

		//Etiquetas
		lblIndice.setText("Indice");
		lblIndice.setVerticalAlignment(JLabel.CENTER);
		lblIndice.setHorizontalAlignment(JLabel.CENTER);
		lblIndice.setBounds(20, 20, 100, 30);//Posicion (X,Y,ANCHO,ALTO)

		lblMencion.setText("Mención");
		lblMencion.setVerticalAlignment(JLabel.CENTER);
		lblMencion.setHorizontalAlignment(JLabel.CENTER);
		//lblContenido.setBackground(Color.blue); para establecer color de fondo
		//lblContenido.setOpaque(true); para establecer color de fondo
		lblMencion.setBounds(200, 20, 100, 30);

		lblNombre.setText("Nombre");
		lblNombre.setVerticalAlignment(JLabel.CENTER);
		lblNombre.setHorizontalAlignment(JLabel.CENTER);
		lblNombre.setBounds(20, 60, 100, 30);

		lblApellido.setText("Apellido");
		lblApellido.setVerticalAlignment(JLabel.CENTER);
		lblApellido.setHorizontalAlignment(JLabel.CENTER);
		lblApellido.setBounds(240, 60, 100, 30);

		//Cajas de texto
		txtIndice.setBounds(140, 20, 50, 30);
	  txtMencion.setBounds(300, 20, 160, 30);
	  txtNombre.setBounds(120, 60, 130, 30);
	  txtApellido.setBounds(340, 60, 120, 30);

		//Tabla del contenido
		conTabla.setBounds(20,100,440,120);

		modTabla.addColumn("Indice");
		modTabla.addColumn("Código");
		modTabla.addColumn("Nombre");
		modTabla.addColumn("Mención");

		tblContenido.setModel(modTabla);
		tblContenido.getTableHeader().setReorderingAllowed(false);	//Evitamos reorganizacion de columnas
		//tblContenido.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		conTabla.setViewportView(tblContenido);

		//Botones
		btnAgregar.setText("AGREGAR");
		btnAgregar.setBounds(20,230,100,35);

		btnModificar.setText("MODIFICAR");
		btnModificar.setBounds(130,230,110,35);

		btnEliminar.setText("ELIMINAR");
		btnEliminar.setBounds(250,230,100,35);

		btnLimpiar.setText("LIMPIAR");
		btnLimpiar.setBounds(360,230,100,35);

		//Agregar eventos a los botones
		btnAgregar.addActionListener(this);
		btnModificar.addActionListener(this);
		btnEliminar.addActionListener(this);
		btnLimpiar.addActionListener(this);
		tblContenido.addMouseListener(this);

		//Agregamos los componentes a la ventana
		this.add(lblIndice);
		this.add(lblMencion);
		this.add(lblNombre);
		this.add(lblApellido);
		this.add(txtIndice);
		this.add(txtMencion);
		this.add(txtNombre);
		this.add(txtApellido);
		this.add(conTabla);
		this.add(btnAgregar);
		this.add(btnModificar);
		this.add(btnEliminar);
		this.add(btnLimpiar);

	}

	public void mouseClicked(MouseEvent e){
		if (e.getSource() == tblContenido){
			//Pasamos los valores de las filas a las cajas de texto con un mouseClicked

			txtIndice.setText(tblContenido.getValueAt(tblContenido.getSelectedRow(), 0).toString());
			txtNombre.setText(tblContenido.getValueAt(tblContenido.getSelectedRow(), 1).toString());
			txtApellido.setText(tblContenido.getValueAt(tblContenido.getSelectedRow(), 2).toString());
			txtMencion.setText(tblContenido.getValueAt(tblContenido.getSelectedRow(), 3).toString());

		}
	}

	//Metodos necesarios para que funciones el click
	public void mouseEntered(MouseEvent e) {}
  	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}


	public void actionPerformed(ActionEvent e) {	//Da error si no se incluye como public
        switch(e.getActionCommand()){
			case "AGREGAR":

				limpiar_tabla();					//Vaciamos la tabla
				registrar_datos(txtNombre.getText(), txtApellido.getText(), txtMencion.getText());
				limpiar_textbox();					//Limpiamos las cajas de texto
				txtNombre.requestFocusInWindow();	//Posicionamos el cursor en la caja de texto nombre
				listarDatosTabla();					//Refrescamos la tabla

			break;
			case "MODIFICAR":

				limpiar_tabla();
				modificar_datos(txtIndice.getText(), txtNombre.getText(), txtApellido.getText(), txtMencion.getText());
				listarDatosTabla();

			break;
			case "ELIMINAR":

				limpiar_tabla();
				remover_datos(txtIndice.getText());
				limpiar_textbox();
				listarDatosTabla();

			break;
			case "LIMPIAR":

				limpiar_textbox();

			break;
			}
    }

	private void listarDatosTabla(){

		for (int i=0;i< array_Estudiantes.size();i++){
			String cadena_completa = array_Estudiantes.get(i);
			String cadena_partes[] = cadena_completa.split(" ");
			Object datos_tabla[] = new Object[4];
			datos_tabla[0]=i;
			datos_tabla[1]=cadena_partes[0];
			datos_tabla[2]=cadena_partes[1];
			datos_tabla[3]=cadena_partes[2];

			modTabla.addRow(datos_tabla);

		}
		tblContenido.setModel(modTabla);
	}

	//Métodos de la clase
	private void registrar_datos(String nombre, String apellido, String mencion){

		if(!(nombre.isEmpty() || apellido.isEmpty() || mencion.isEmpty())){	//Valida que las cajas de texto no esten vacias
			array_Estudiantes.add(nombre+" "+apellido+" "+mencion);						//Agregamos los registros
		}else{
			JOptionPane.showMessageDialog(null,"No pueden quedar cajas de texto vacías");
		}

	}

	private void modificar_datos(String posicion_fila, String nombre, String apellido, String mencion){

		if(!(posicion_fila.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || mencion.isEmpty())){	 //Valida que las cajas de texto no esten vacias
			array_Estudiantes.set(Integer.parseInt(posicion_fila), nombre+" "+apellido+" "+mencion);										 			//Modificamos los registros
		}else{
			JOptionPane.showMessageDialog(null,"Haga click en la fila a modificar");
		}

	}

	private void remover_datos(String posicion_fila){

		if(!posicion_fila.isEmpty()){
			array_Estudiantes.remove(Integer.parseInt(posicion_fila));
		}else{
			JOptionPane.showMessageDialog(null, "Debe introducir el indice del registro a eliminar");
			txtIndice.requestFocusInWindow();
		}
	}

	private void limpiar_tabla(){
		modTabla.setRowCount(0);
		tblContenido.setModel(modTabla);
	}

	private void limpiar_textbox(){
		txtIndice.setText("");
		txtMencion.setText("");
		txtNombre.setText("");
		txtApellido.setText("");

		txtIndice.requestFocusInWindow();
	}

	public static void main(String[] args){

		ArrayJava iniciar = new ArrayJava();
		iniciar.setVisible(true);

	}
}
